package ru.nizhikov.carggo;

import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;

/**
 * @author NIzhikov
 */
public class IslandFinder {
	private static final char EARTH = '*';
	private static final char WATER = '~';
	private static final char VISITED_EARTH = '!';


	/**
	 * Остров — набор квадратных клеток суши, где из каждой клетки можно добраться до любой другой.
	 * Остров полностью омывается водой.
	 * Острова не могут соприкасаться по диагонали.
	 * На острове нет озёр, то есть внутри острова не может быть никакой воды.
	 *
	 * @param map
	 * @return
	 */
	public int findIslands(char[][] map) {
		int islandCount = 0;
		for (int i = 0; i < map.length; i++) {
			char[] row = map[i];
			for (int j = 0; j < row.length; j++) {
				if (map[i][j] == EARTH) {
					islandCount++;
					markIsland(map, i, j);
				}
			}
		}
		restoreState(map);
		return islandCount;
	}

	private void restoreState(char[][] map) {
		for (int i = 0; i < map.length; i++) {
			char[] row = map[i];
			for (int j = 0; j < row.length; j++) {
				if (map[i][j] == VISITED_EARTH) {
					map[i][j] = EARTH;
				}
			}
		}
	}

	private void markIsland(char[][] map, int i, int j) {
		map[i][j] = VISITED_EARTH;
		if (i > 0 && map[i - 1][j] == EARTH)
			markIsland(map, i - 1, j);

		if (i < map.length - 1 && map[i + 1][j] == EARTH)
			markIsland(map, i + 1, j);

		if (j > 0 && map[i][j - 1] == EARTH)
			markIsland(map, i, j - 1);

		if (j < map[i].length - 1 && map[i][j + 1] == EARTH)
			markIsland(map, i, j + 1);
	}
}
