package ru.nizhikov.carggo;

import org.junit.Assert;
import org.junit.Test;

import java.util.Scanner;

/**
 * @author NIzhikov
 */
public class IslandFinderTest extends Assert {
	@Test
	public void test1() throws Exception {
		testFromFile("test1.txt");
	}

	@Test
	public void test2() throws Exception {
		testFromFile("test2.txt");
	}

	@Test
	public void test3() throws Exception {
		testFromFile("test3.txt");
	}

	@Test
	public void test4() throws Exception {
		testFromFile("test4.txt");
	}

	@Test
	public void test5() throws Exception {
		testFromFile("test5.txt");
	}

	@Test
	public void test6() throws Exception {
		testFromFile("test6.txt");
	}

	@Test
	public void test7() throws Exception {
		testFromFile("test7.txt");
	}

	@Test
	public void test8() throws Exception {
		testFromFile("test8.txt");
	}

	@Test
	public void test9() throws Exception {
		testFromFile("test9.txt");
	}

	@Test
	public void test10() throws Exception {
		testFromFile("test10.txt");
	}

	@Test
	public void test11() throws Exception {
		//Мне было лень делать итерацию по ресурсам, поэтому тесты сделал copy-paste :)
		//Надеюсь, это сильно не огорчит проверяющего
		testFromFile("test11.txt");
	}

	private void testFromFile(String fileName) {
		Scanner sc = new Scanner(this.getClass().getResourceAsStream("/" + fileName));

		//Сознательно игнорирую проверку правильности формата тестового файла.
		//При нормальном тесте её надо прикрутить
		String heightAndWidht = sc.nextLine();
		int height = Integer.valueOf(heightAndWidht.substring(0, heightAndWidht.indexOf(' ')));
		int width = Integer.valueOf(heightAndWidht.substring(heightAndWidht.indexOf(' ') + 1));

		char[][] map = new char[height][width];
		for (int i = 0; i < height; i++) {
			String line = sc.nextLine();
			line.getChars(0, width, map[i], 0);
		}

		long answer = Long.valueOf(sc.nextLine());
		assertEquals(answer, new IslandFinder().findIslands(map));
	}
}
